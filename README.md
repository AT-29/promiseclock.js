# promiseclock.js


>simple example 
```javascript
const timer = promiseTimer()
const queue = timer.priorityQueue(88); //overall delay of 88 ms
queue.add(()=>fetch('https://jsonplaceholder.typicode.com/todos/1').then((data)=>data.json()), (data)=>console.log(data.id,data.title))//+0 delay
queue.add(()=>fetch('https://jsonplaceholder.typicode.com/todos/2').then((data)=>data.json()), (data)=>console.log(data.id,data.title))//+0 delay
queue.add(()=>fetch('https://jsonplaceholder.typicode.com/todos/3').then((data)=>data.json()), (data)=>console.log(data.id,data.title))//+0 delay
queue.add(()=>fetch('https://jsonplaceholder.typicode.com/todos/4').then((data)=>data.json()), (data)=>console.log(data.id,data.title),100)//+100 delay
queue.add(()=>fetch('https://jsonplaceholder.typicode.com/todos/5').then((data)=>data.json()), (data)=>console.log(data.id,data.title),200)//+200 delay
queue.add(()=>fetch('https://jsonplaceholder.typicode.com/todos/6').then((data)=>data.json()), (data)=>console.log(data.id,data.title),300)//+200 delay
queue.resolve()
```

>in Node.js with console user input
```javascript
const fetch = require("node-fetch");
const timer = promiseClock()
const readline = require("readline");
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question("Please enter desired speed\n", (speed)=> {
  const queue = timer.priorityQueue(+speed);
    rl.question("How many todos you want to get? [1-200]\n", (n)=> {
      const len = Math.min(200,n)
      for(let i = 1; i<=len;i+=1){
        queue.add(()=>fetch('https://jsonplaceholder.typicode.com/todos/'+i).then((data)=>data.json()), (data)=>console.log(i,JSON.stringify(data)))
      }
      queue.onEnd(()=> rl.close())
      queue.resolve()
 
    });
});

rl.on("close", ()=> {
    console.log("\nJob's done!");
    process.exit(0);
});
```
