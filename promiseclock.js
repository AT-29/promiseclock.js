
const promiseClock = () => {
  const priorityQueue = (interval=0) =>{
    let queueCount = -1;
    const queue = [];
    const delays = [];
      const enque = (order,promise,callback,handler=(error)=>console.log(error)) => {
        queueCount+=1;
        order=queueCount;
        return (async () => {
        try{
        const data = await promise();
        return data;
        }catch(err){
          handler(err);
        }
      })().then(res=>{
        queue[order] = () => callback(res);
      });
      }
      let finishCallback = ()=>true;
      const onEnd = (callback) =>{
        finishCallback = callback;
      }
      const resolve = (count=0) =>{
        if(count<=queueCount){
          setTimeout(()=>{
            if(queue[count]){
              queue[count]()
              queue[count] = null;
              count+=1;
            }
            return resolve(count)
        },delays[count]+interval);
        }else{
          finishCallback();
        }
      }
      let count = 0;
      const add = (promise,callback,delay=0)=>{
          enque(count,promise, callback)
          delays[count] = delay;
          count+=1;
      }
      return {add,resolve,onEnd}
  }
  return {priorityQueue}
};
